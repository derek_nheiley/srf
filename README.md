![Alt test](/SRF.png?raw=true "SRF Logo")

Simple Responsive Folder (SRF) combines several free themes from Luis Zuno (aka Ansimuz) to create a mashup from his Simpler, and Folder templates with responsive aspects added from other themes.

A bit more PHP will be used to modularize repetitive code, and let me do some server side stuff for various project pages.